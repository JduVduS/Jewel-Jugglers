using System;                                   // System contains a lot of default C# libraries 
using GXPEngine;                                // GXPEngine contains the engine
using System.Drawing;
using System.Collections.Generic;                           // System.Drawing contains drawing tools such as Color definitions
using System.IO.Ports;
using System.Linq;
using System.Globalization;
using System.Net;
using System.Runtime.CompilerServices;

public class MyGame : Game {
    TiltPlatform tiltPlatform;
    ArduinoControls arduinoControls;
    public MyGame() : base(800, 800, false,false,-1,-1,true)     // Create a window that's 800x600 and NOT fullscreen
	{
        targetFps = 30; // Consistent, non variable framerate

        CreateBackground();
		tiltPlatform = new TiltPlatform();
        arduinoControls = new ArduinoControls();
		AddChild(tiltPlatform);
        AddChild(arduinoControls);
    }

	// For every game object, Update is called every frame, by the engine:
	void Update()
	{
        //Console.WriteLine(game.currentFps);
        arduinoControls.UseFile(tiltPlatform);
    }

    void CreateBackground()
    {
        EasyDraw canvas = new EasyDraw(width, height, false);
        canvas.Fill(50, 50, 150);
        canvas.NoStroke();
        canvas.ShapeAlign(CenterMode.Min, CenterMode.Min);
        canvas.Rect(0, 0, width, height);
        AddChild(canvas);
    }

    static void Main()                          // Main() is the first method that's called when the program is run
	{
        // Changing region decimal separator so parsing read data is easier.
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        new MyGame().Start();                   // Create a "MyGame" and start it
	}
}