﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using GXPEngine;

class TiltPlatform : Sprite
{
    public TiltPlatform() : base("Assets/greenButton.png")
    {
        SetOrigin(width / 2, height / 2);
        x = 400;
        y = 400;
    }
}
